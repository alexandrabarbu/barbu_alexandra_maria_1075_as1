
const FIRST_NAME = "Alexandra Maria";
const LAST_NAME = "Barbu";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if (isFinite(value) && value <= Number.MAX_SAFE_INTEGER && value >= Number.MIN_SAFE_INTEGER)
        return parseInt(value);
    else
        return NaN;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}
